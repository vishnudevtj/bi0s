* 2017
** 2017-08 August
*** 2017-08-09 Wednesday
**** Report: 

1. Solved One problem from exercism
2. Trying to solve problems from pico CTF
*** 2017-08-29 Tuesday
**** Report :
1. Solved [[https://2017game.picoctf.com/game/level-3/challenge/Coffee][Coffee]]  Writeup : [[file:pico2017/pico.org::*Coffee][Coffee]]
*** 2017-08-30 Wednesday
**** Report : 
1. Solved 2 exercism
2. Wrote documented solution for  Pico https://gitlab.com/vishnudevtj/bi0s
** 2017-09 September
*** 2017-09-07 Thursday
**** Report : 
inctf Quater 2

Reversing 2 

#+BEGIN_SRC python :results output org drawer
def xor(x):
    key = 10
    n = ""
    for i in x:
        n += chr(ord(i) ^ key)

    return n


def shift(x):
    m = list(x)
    n = []
    for i in m:
        if(ord(i) >= 65 and ord(i) <= 90):
            if ord(i) == 88:
                n.append('A')
            elif ord(i) == 89:
                n.append('B')
            elif ord(i) == 90:
                n.append('C')
            else:
                n.append(chr((ord(i) + 3)))

        elif(ord(i) >= 97 and ord(i) <= 122):
            if ord(i) == 120:
                n.append('a')
            elif ord(i) == 121:
                n.append('b')
            elif ord(i) == 122:
                n.append('c')
            else:
                n.append(chr((ord(i) + 3)))
        elif(ord(i) >= 48 and ord(i) <= 57):
            n.append(str((int(i) + 3) % 10))
    return n


def encode(x):
    return x.encode("hex")


from itertools import chain
flag = ""
cipher = "\x66\x7b\x6c\x7d\x63\x67\x7f\x3c\x73\x3c\x7f\x32\x3c\x3c\x7b\x33\x3e\x7b\x3c\x3c\x7f\x3e\x7b\x33\x3e\x32\x32\x39\x3c\x3d\x32\x68"
while flag != cipher:
    for i in chain(range(65, 91), range(97, 123), range(48, 58)):
        a = shift(chr(i))  # ROT 3
        c = xor(a)
        print c, cipher[len(flag)]

        if c == cipher[len(flag)]:
            flag += chr(i)
        print(flag)
#+END_SRC


Wizard-free-vault 

#+BEGIN_SRC python :results output org drawer
import hashlib
import struct
import re as sbc
from itertools import chain

obj_1 = hashlib.md5()
abc = 12132
str1 = ''
str2 = ''
xorer = [117, 5, 36, 94, 6, 23, 113, 44, 71, 105, 6, 125, 1, 6, 90, 31, 54, 57, 117,
         118, 2, 123, 61, 112, 1, 74, 106, 98, 69, 2, 12, 93]

inp1 = "Muggles"  # Online MD5 Cracker 8a0e2e33fc0ef5f4d1b9243139a2aa4f
cipher = "=:P3_7R"
flag = []
for j in inp1:
    for i in range(1, 1000):
        if chr(ord(j) ^ int(i)) == cipher[len(flag)]:
            flag.append(i)
            break


print(flag)

inp3 = QldeQlde27Qlde50
inp4 = Qlde


#+END_SRC
*** 2017-09-08 Friday
**** Report :
*** 2017-09-10 Sunday
**** Report : 

Solved NUmber Trubble 

#+BEGIN_SRC python :results output org drawer
from PIL import Image

cor =  [(255, 255, 255, 255), ... ]
img = Image.new('RGB',(200,200),'white')
pixels = img.load()
cordinates = []

for i in range(0,len(cor),200):
    cordinates.append(cor[i:i+200])

for i in range(0,200):
    for j,n in zip( cordinates[i],range(0,200)):
        pixels[i,n]=j[0:3]
img.show()
img.save("image.png")

#+END_SRC
*** 2017-09-12 Tuesday
**** Report :
1.I wrote the assembly code , but could not push to git
*** 2017-09-16 Saturday
**** Report : 

1. Participated in https://mitrestemctf.org
   Solved a Forensic 100 
      I Used binwalk to extrat the PNG file from given files
 2. Assembly Codes @ https://gitlab.com/vishnudevtj/bi0s/tree/master/AssemblyLanguage
*** 2017-09-19 Tuesday
**** Report : 
1. For the Past three days I solved Reverse Engeneering problems from
InCTF 
    1. Easy Windows Crackme - 150
    2. Wizard-free-vault - 200
    3. Recall - 250
    4. Vault - 250
2. Today I Read the book Practical Reverse Engineering
*** 2017-09-20 Wednesday
**** Report : 
1. Read the book Practical Reverse Engineering
2. Learned some elisp
*** 2017-09-21 Thursday
**** Report : 
***** Solved CSAW '17 Reversing tablEZ - 100

what the binary does is that it transposses the input from a predefined table and checks the result with another string 
We have to find the correct input which is the flag , 

#+BEGIN_SRC python :results output org drawer

# Dump of the table # 0x555555755280 <trans_tbl>
# they are in a key value pairs and the input is transpossed according
# to this table 

table = [0x01,	0xbb,	0x02,	0x9b,	0x03,	0xc4,	0x04,	0x6c,
	0x05,	0x4a,	0x06,	0x2e,	0x07,	0x22,	0x08,	0x45,
	0x09,	0x33,	0x0a,	0xb8,	0x0b,	0xd5,	0x0c,	0x06,
	0x0d,	0x0a,	0x0e,	0xbc,	0x0f,	0xfa,	0x10,	0x79,
	0x11,	0x24,	0x12,	0xe1,	0x13,	0xb2,	0x14,	0xbf,
	0x15,	0x2c,	0x16,	0xad,	0x17,	0x86,	0x18,	0x60,
	0x19,	0xa4,	0x1a,	0xb6,	0x1b,	0xd8,	0x1c,	0x59,
	0x1d,	0x87,	0x1e,	0x41,	0x1f,	0x94,	0x20,	0x77,
	0x21,	0xf0,	0x22,	0x4f,	0x23,	0xcb,	0x24,	0x61,
	0x25,	0x25,	0x26,	0xc0,	0x27,	0x97,	0x28,	0x2a,
	0x29,	0x5c,	0x2a,	0x08,	0x2b,	0xc9,	0x2c,	0x9f,
	0x2d,	0x43,	0x2e,	0x4e,	0x2f,	0xcf,	0x30,	0xf9,
	0x31,	0x3e,	0x32,	0x6f,	0x33,	0x65,	0x34,	0xe7,
	0x35,	0xc5,	0x36,	0x39,	0x37,	0xb7,	0x38,	0xef,
	0x39,	0xd0,	0x3a,	0xc8,	0x3b,	0x2f,	0x3c,	0xaa,
	0x3d,	0xc7,	0x3e,	0x47,	0x3f,	0x3c,	0x40,	0x81,
	0x41,	0x32,	0x42,	0x49,	0x43,	0xd3,	0x44,	0xa6,
	0x45,	0x96,	0x46,	0x2b,	0x47,	0x58,	0x48,	0x40,
	0x49,	0xf1,	0x4a,	0x9c,	0x4b,	0xee,	0x4c,	0x1a,
	0x4d,	0x5b,	0x4e,	0xc6,	0x4f,	0xd6,	0x50,	0x80,
	0x51,	0x2d,	0x52,	0x6d,	0x53,	0x9a,	0x54,	0x3d,
	0x55,	0xa7,	0x56,	0x93,	0x57,	0x84,	0x58,	0xe0,
	0x59,	0x12,	0x5a,	0x3b,	0x5b,	0xb9,	0x5c,	0x09,
	0x5d,	0x69,	0x5e,	0xba,	0x5f,	0x99,	0x60,	0x48,
	0x61,	0x73,	0x62,	0xb1,	0x63,	0x7c,	0x64,	0x82,
	0x65,	0xbe,	0x66,	0x27,	0x67,	0x9d,	0x68,	0xfb,
	0x69,	0x67,	0x6a,	0x7e,	0x6b,	0xf4,	0x6c,	0xb3,
	0x6d,	0x05,	0x6e,	0xc2,	0x6f,	0x5f,	0x70,	0x1b,
	0x71,	0x54,	0x72,	0x23,	0x73,	0x71,	0x74,	0x11,
	0x75,	0x30,	0x76,	0xd2,	0x77,	0xa5,	0x78,	0x68,
	0x79,	0x9e,	0x7a,	0x3f,	0x7b,	0xf5,	0x7c,	0x7a,
	0x7d,	0xce,	0x7e,	0x0b,	0x7f,	0x0c,	0x80,	0x85,
	0x81,	0xde,	0x82,	0x63,	0x83,	0x5e,	0x84,	0x8e,
	0x85,	0xbd,	0x86,	0xfe,	0x87,	0x6a,	0x88,	0xda,
	0x89,	0x26,	0x8a,	0x88,	0x8b,	0xe8,	0x8c,	0xac,
	0x8d,	0x03,	0x8e,	0x62,	0x8f,	0xa8,	0x90,	0xf6,
	0x91,	0xf7,	0x92,	0x75,	0x93,	0x6b,	0x94,	0xc3,
	0x95,	0x46,	0x96,	0x51,	0x97,	0xe6,	0x98,	0x8f,
	0x99,	0x28,	0x9a,	0x76,	0x9b,	0x5a,	0x9c,	0x91,
	0x9d,	0xec,	0x9e,	0x1f,	0x9f,	0x44,	0xa0,	0x52,
	0xa1,	0x01,	0xa2,	0xfc,	0xa3,	0x8b,	0xa4,	0x3a,
	0xa5,	0xa1,	0xa6,	0xa3,	0xa7,	0x16,	0xa8,	0x10,
	0xa9,	0x14,	0xaa,	0x50,	0xab,	0xca,	0xac,	0x95,
	0xad,	0x92,	0xae,	0x4b,	0xaf,	0x35,	0xb0,	0x0e,
	0xb1,	0xb5,	0xb2,	0x20,	0xb3,	0x1d,	0xb4,	0x5d,
	0xb5,	0xc1,	0xb6,	0xe2,	0xb7,	0x6e,	0xb8,	0x0f,
	0xb9,	0xed,	0xba,	0x90,	0xbb,	0xd4,	0xbc,	0xd9,
	0xbd,	0x42,	0xbe,	0xdd,	0xbf,	0x98,	0xc0,	0x57,
	0xc1,	0x37,	0xc2,	0x19,	0xc3,	0x78,	0xc4,	0x56,
	0xc5,	0xaf,	0xc6,	0x74,	0xc7,	0xd1,	0xc8,	0x04,
	0xc9,	0x29,	0xca,	0x55,	0xcb,	0xe5,	0xcc,	0x4c,
	0xcd,	0xa0,	0xce,	0xf2,	0xcf,	0x89,	0xd0,	0xdb,
	0xd1,	0xe4,	0xd2,	0x38,	0xd3,	0x83,	0xd4,	0xea,
	0xd5,	0x17,	0xd6,	0x07,	0xd7,	0xdc,	0xd8,	0x8c,
	0xd9,	0x8a,	0xda,	0xb4,	0xdb,	0x7b,	0xdc,	0xe9,
	0xdd,	0xff,	0xde,	0xeb,	0xdf,	0x15,	0xe0,	0x0d,
	0xe1,	0x02,	0xe2,	0xa2,	0xe3,	0xf3,	0xe4,	0x34,
	0xe5,	0xcc,	0xe6,	0x18,	0xe7,	0xf8,	0xe8,	0x13,
	0xe9,	0x8d,	0xea,	0x7f,	0xeb,	0xae,	0xec,	0x21,
	0xed,	0xe3,	0xee,	0xcd,	0xef,	0x4d,	0xf0,	0x70,
	0xf1,	0x53,	0xf2,	0xfd,	0xf3,	0xab,	0xf4,	0x72,
	0xf5,	0x64,	0xf6,	0x1c,	0xf7,	0x66,	0xf8,	0xa9,
	0xf9,	0xb0,	0xfa,	0x1e,	0xfb,	0xd7,	0xfc,	0xdf,
	0xfd,	0x36,	0xfe,	0x7d,	0xff,	0x31]

# Created a dictionart which corresponds the value and key

table = {table[i+1]:table[i] for i in range(0,len(table),2)}

# 0x5555555549f7 strcmp compares the transpossed string this value

key = [0x27,	0xb3,	0x73,	0x9d,	0xf5,	0x11,	0xe7,	0xb1,
	0xb3,	0xbe,	0x99,	0xb3,	0xf9,	0xf9,	0xf4,	0x30,
	0x1b,	0x71,	0x99,	0x73,	0x23,	0x65,	0x99,	0xb1,
	0x65,	0x11,	0x11,	0xbe,	0x23,	0x99,	0x27,	0xf9,
	0x23,	0x99,	0x05,	0x65,	0xce,]

# reverses the key  and print the flag

print(''.join([chr(table[i]) for i in key]))

#+END_SRC

Flag : 
#+BEGIN_QUOTE
flag{t4ble_l00kups_ar3_b3tter_f0r_m3}
#+END_QUOTE
*** 2017-09-24 Sunday
**** Report : Bomb Labs 1
[[http://www.csapp.cs.cmu.edu/public/labs.html][bomb_labs]]
Phase 1 :
It checks the input with a string in the binary , which is
#+BEGIN_QUOTE
Border relations with Canada have never been better.
#+END_QUOTE
Phase 2 
Return value of scanf is the no of values matched or input and the no of input should be 6
The inputs are 
#+BEGIN_SRC python :results output org drawer

input = [1 ,2, 4, 8, 16, 32]
if input[0]==1:
    for i in range(1,len(input)):
        if input[i-1]+input[i-1] == input[i]:
            continue
        else:
            print("domb!!!")

#+END_SRC

Phase 3 
Inputs two numbers and checks it is 7 and 327
Phase 4
It reads two inputs gives the first input to func4 it is a recursive function and the eax register value should be 0 , when the input's are not
7 , 6 ,3 ,0 
The second input should be 0

Phase 5
it Reads 6 char strings and calculate logical and operaton on the letters and the result is used as an index for the substitution array  'maduiersnfotvbyl'
so the input is 'IONEFG'

Phase 6 

input : 6 chr 
input[0] != input[1:6]

set {int} 0x603320 = 477
set {int} 0x603310 = 477
set {int} 0x6032d0 = 477
set {int} 0x6032e0 = 477
set {int} 0x6032f0 = 477
set {int} 0x603300 = 477
*** 2017-09-25 Monday
**** Report : 
***** Solved Backdoor 2017 image rev 250 


The challenge gives you two files One ~encrypt.py~ and ~enctrypted.txt~
#+BEGIN_SRC python :results output org drawer
from PIL import Image


def bin_return(dec):
    return(str(format(dec, 'b')))


def bin_8bit(dec):
    return(str(format(dec, '08b')))


def convert_32bit(dec):
    return(str(format(dec, '032b')))


def convert_64bit(dec):
    return(str(format(dec, '064b')))


def hex_return(dec):
    return expand(hex(dec).replace('0x', '').replace('L', ''))


def dec_return_bin(bin_string):
    return(int(bin_string, 2))


def dec_return_hex(hex_string):
    return(int(hex_string, 16))


def some_LP(l, n):
    l1 = []
    j = 0
    k = n
    while k < len(l) + 1:
        l1.append(l[j:k])
        j = k
        k += n
    return(l1)


def rotate_right(bit_string, n):
    bit_list = list(bit_string)
    count = 0
    while count <= n - 1:
        list_main = list(bit_list)
        var_0 = list_main.pop(-1)
        list_main = list([var_0] + list_main)
        bit_list = list(list_main)
        count += 1
    return(''.join(list_main))


def shift_right(bit_string, n):
    bit_list = list(bit_string)
    count = 0
    while count <= n - 1:
        bit_list.pop(-1)
        count += 1
    front_append = ['0'] * n
    return(''.join(front_append + bit_list))


def addition(input_set):
    value = 0
    for i in range(len(input_set)):
        value += input_set[i]
    mod_32 = 4294967296
    return(value % mod_32)


def str_xor(s1, s2):
    return ''.join([str(int(i) ^ int(j)) for i, j in zip(s1, s2)])


def str_and(s1, s2):
    return ''.join([str(int(i) & int(j)) for i, j in zip(s1, s2)])


def str_not(s):
    return ''.join([str(int(i) ^ 1) for i in s])


def not_and_and_xor(x, y, z):
    return(str_xor(str_and(x, y), str_and(str_not(x), z)))


def and_and_and_xor_xor(x, y, z):
    return(str_xor(str_xor(str_and(x, y), str_and(x, z)), str_and(y, z)))


def some_e0(x):
    return(str_xor(str_xor(rotate_right(x, 2), rotate_right(x, 13)), rotate_right(x, 22)))


def some_e1(x):
    return(str_xor(str_xor(rotate_right(x, 6), rotate_right(x, 11)), rotate_right(x, 25)))


def some_s0(x):
    return(str_xor(str_xor(rotate_right(x, 7), rotate_right(x, 18)), shift_right(x, 3)))


def some_s1(x):
    return(str_xor(str_xor(rotate_right(x, 17), rotate_right(x, 19)), shift_right(x, 10)))


def expand(s):
    return '0' * (8 - len(s)) + s


def get_pixels_list(filename):
    im = Image.open(filename)
    return list(im.getdata())


def data_encrypted(list_of_pixels):
    data = ''
    for i in list_of_pixels:
        d = ''.join([chr(j) for j in i])
        d = encryption(d)
        data += ''.join(d)
    return data


def message_pad(bit_list):
    pad_one = bit_list + '1'
    pad_len = len(pad_one)
    k = 0
    while ((pad_len + k) - 448) % 512 != 0:
        k += 1
    back_append_0 = '0' * k
    back_append_1 = convert_64bit(len(bit_list))
    return(pad_one + back_append_0 + back_append_1)


def message_bit_return(string_input):
    bit_list = []
    for i in range(len(string_input)):
        bit_list.append(bin_8bit(ord(string_input[i])))
    return(''.join(bit_list))


def message_pre_pro(input_string):
    bit_main = message_bit_return(input_string)
    return(message_pad(bit_main))


def message_parsing(input_string):
    return(some_LP(message_pre_pro(input_string), 32))


def message_schedule(index, w_t):
    new_word = convert_32bit(addition([int(some_s1(w_t[index - 2]), 2), int(
        w_t[index - 7], 2), int(some_s0(w_t[index - 15]), 2), int(w_t[index - 16], 2)]))
    return(new_word)


initial = ['6a09e667', 'bb67ae85', '3c6ef372', 'a54ff53a',
           '510e527f', '9b05688c', '1f83d9ab', '5be0cd19']

values = ['428a2f98', '71374491', 'b5c0fbcf', 'e9b5dba5', '3956c25b', '59f111f1', '923f82a4', 'ab1c5ed5', 'd807aa98', '12835b01', '243185be', '550c7dc3', '72be5d74', '80deb1fe', '9bdc06a7', 'c19bf174', 'e49b69c1', 'efbe4786', '0fc19dc6', '240ca1cc', '2de92c6f', '4a7484aa', '5cb0a9dc', '76f988da', '983e5152', 'a831c66d', 'b00327c8', 'bf597fc7', 'c6e00bf3', 'd5a79147', '06ca6351', '14292967',
          '27b70a85', '2e1b2138', '4d2c6dfc', '53380d13', '650a7354', '766a0abb', '81c2c92e', '92722c85', 'a2bfe8a1', 'a81a664b', 'c24b8b70', 'c76c51a3', 'd192e819', 'd6990624', 'f40e3585', '106aa070', '19a4c116', '1e376c08', '2748774c', '34b0bcb5', '391c0cb3', '4ed8aa4a', '5b9cca4f', '682e6ff3', '748f82ee', '78a5636f', '84c87814', '8cc70208', '90befffa', 'a4506ceb', 'bef9a3f7', 'c67178f2']


def encryption(input_string):
    w_t = message_parsing(input_string)
    a = convert_32bit(dec_return_hex(initial[0]))
    b = convert_32bit(dec_return_hex(initial[1]))
    c = convert_32bit(dec_return_hex(initial[2]))
    d = convert_32bit(dec_return_hex(initial[3]))
    e = convert_32bit(dec_return_hex(initial[4]))
    f = convert_32bit(dec_return_hex(initial[5]))
    g = convert_32bit(dec_return_hex(initial[6]))
    h = convert_32bit(dec_return_hex(initial[7]))
    for i in range(0, 64):
        if i <= 15:
            t_1 = addition([int(h, 2), int(some_e1(e), 2), int(
                not_and_and_xor(e, f, g), 2), int(values[i], 16), int(w_t[i], 2)])
            t_2 = addition([int(some_e0(a), 2), int(
                and_and_and_xor_xor(a, b, c), 2)])
            h = g
            g = f
            f = e
            e = addition([int(d, 2), t_1])
            d = c
            c = b
            b = a
            a = addition([t_1, t_2])
            a = convert_32bit(a)
            e = convert_32bit(e)
        if i > 15:
            w_t.append(message_schedule(i, w_t))
            t_1 = addition([int(h, 2), int(some_e1(e), 2), int(
                not_and_and_xor(e, f, g), 2), int(values[i], 16), int(w_t[i], 2)])
            t_2 = addition([int(some_e0(a), 2), int(
                and_and_and_xor_xor(a, b, c), 2)])
            h = g
            g = f
            f = e
            e = addition([int(d, 2), t_1])
            d = c
            c = b
            b = a
            a = addition([t_1, t_2])
            a = convert_32bit(a)
            e = convert_32bit(e)
    value_0 = addition([dec_return_hex(initial[0]), int(a, 2)])
    value_1 = addition([dec_return_hex(initial[1]), int(b, 2)])
    value_2 = addition([dec_return_hex(initial[2]), int(c, 2)])
    value_3 = addition([dec_return_hex(initial[3]), int(d, 2)])
    value_4 = addition([dec_return_hex(initial[4]), int(e, 2)])
    value_5 = addition([dec_return_hex(initial[5]), int(f, 2)])
    value_6 = addition([dec_return_hex(initial[6]), int(g, 2)])
    value_7 = addition([dec_return_hex(initial[7]), int(h, 2)])
    value = (hex_return(value_0), hex_return(value_1), hex_return(value_2), hex_return(
        value_3), hex_return(value_4), hex_return(value_5), hex_return(value_6), hex_return(value_7))
    return(value)

list_pixels = get_pixels_list('./flag.png')
data = data_encrypted(list_pixels)
f = open('./encrypted.txt','w')
f.write(data)
f.close()
#+END_SRC

encrypted.txt contains the hashed output
#+BEGIN_EXAMPLE
709e80c88487a2411e1ee4dfb9f22a861492d20c4765150c0c794abd70f8147c709e80c88487a2 ......
#+END_EXAMPLE

and the text file containing the encrypted data . 

First I tried to Reverse the Code and it was booting , Got some ideas about the script that it takes the tuple of the pixel values and
convert it to a hash of 64 length 

So I Taught of Brute forcing the entire space , which is ~256x256x256~

The only Problem is that it takes time, it takes 1sec in my pc to calculate 10 hashes and the whole will take 2 days ,which is not possible

#+BEGIN_SRC python :results output org drawer
f = open('./encrypted.txt', 'r'),
text = f.read()
enc = [text[i:i + 64] for i in range(0, len(text), 64)]
l1 = open('./list.txt','w')
for i in enc:
    l1.write(i+"\n")
#+END_SRC
#+BEGIN_SRC shell
cat list.txt   | sort -n | uniq -c | sort -n -r
#+END_SRC
#+BEGIN_EXAMPLE
5821 709e80c88487a2411e1ee4dfb9f22a861492d20c4765150c0c794abd70f8147c
 306 ac205167ca956b408a925c3854fdd82ffa43672263ae7dba5a68b29d9a81fa56
 291 2ec847d8a31a988b3117a5095dae74f490448223f035ec7eddef6768b91a9028
 188 8ae40a3583aef6697d2c2eff57eb915ed0bda54aaa92812ad97982743ac06f37
  90 ab5ab0fedc83e5a1a1871c427eccbcd3cf0fc1bb74a82a552adfd9b4e57f391b
  85 2ac9a6746aca543af8dff39894cfe8173afba21eb01c6fae33d52947222855ef
  79 f1b901847390b0ed7e374e7c1e464ec17b46a427c487a5ad6cbd2906405083d5
  73 5ae7e6a42304dc6e4176210b83c43024f99a0bce9a870c3b6d2c95fc8ebfb74c
  62 b9e8d0a22760b87553c0b9c55ae93058bf8d4389c87765488cea1637e94bd9b6
  59 a30cb1d8569c5c141b2ade1caf57038b2be46c9bc4939c8f702a0ff4fcecfd77
  58 91737e71235959a56c524997e18d6d14d6ddd714ed2a450a24f765255a2733ee
  53 700af1feb55ab0613bdbc466815643743156af4e869120244eb05ca72c45002c
  50 0aad7da77d2ed59c396c99a74e49f3a4524dcdbcb5163251b1433d640247aeb4
  47 7b108f7c5c6f1507c4ffe2275dd9b8e25a71d175a5a9d3e19aeec3f27d82caf1
  42 204164d223b35aabb54ea32b1d14d8bb5a8df56f7c81f3304987fa4193426729
  38 c4289629b08bc4d61411aaa6d6d4a0c3c5f8c1e848e282976e29b6bed5aeedc7
  24 5ae0d5195906bfc4f70167cf171ae4d08e7376aa246977acf172187d5d384f10
#+END_EXAMPLE

I spilt the encrypted.txt into fragments of length 64 and analysing the files reveals that there are only 17 uniq values

So I grabbed the most used used RGB colors from [[http://www.cloford.com/resources/colours/500col.htm][here]]

#+BEGIN_SRC python :results output org drawer

color_list = [(176, 	23, 	31),
              (220, 	20, 	60),
              (255, 	182, 	193),
              (255, 	174, 	185),
              (238, 	162, 	173),
              (205, 	140, 	149),
              (139, 	95, 	101),
              (255, 	192, 	203),
              (255, 	181, 	197),
              ...
              ...
              ...
]

for i in range(0, len(color_list), 1):
    if data_encrypted(color_list[i:i + 1]) in col_hash:
        print(color_list[i:i + 1], data_encrypted(color_list[i:i + 1]))
#+END_SRC
#+BEGIN_EXAMPLE
([(255, 255, 255)], '5ae7e6a42304dc6e4176210b83c43024f99a0bce9a870c3b6d2c95fc8ebfb74c')
([(128, 128, 128)], '8ae40a3583aef6697d2c2eff57eb915ed0bda54aaa92812ad97982743ac06f37')
([(0, 0, 0)], '709e80c88487a2411e1ee4dfb9f22a861492d20c4765150c0c794abd70f8147c')
([(207, 207, 207)], '7b108f7c5c6f1507c4ffe2275dd9b8e25a71d175a5a9d3e19aeec3f27d82caf1')
([(191, 191, 191)], 'ac205167ca956b408a925c3854fdd82ffa43672263ae7dba5a68b29d9a81fa56')
([(143, 143, 143)], 'b9e8d0a22760b87553c0b9c55ae93058bf8d4389c87765488cea1637e94bd9b6')
([(112, 112, 112)], 'c4289629b08bc4d61411aaa6d6d4a0c3c5f8c1e848e282976e29b6bed5aeedc7')
([(64, 64, 64)], '2ec847d8a31a988b3117a5095dae74f490448223f035ec7eddef6768b91a9028')
([(48, 48, 48)], '2ac9a6746aca543af8dff39894cfe8173afba21eb01c6fae33d52947222855ef')
#+END_EXAMPLE

The output shows that the pixels have all the values equal , Let's test all the values from 0 - 256

#+BEGIN_SRC python :results output org drawer

for i in range(0, 256):
    if data_encrypted([(i, i, i)]) in col_hash:
        print(i, data_encrypted([(i, i, i)]))
#+END_SRC
#+BEGIN_EXAMPLE

Result :
(0, '709e80c88487a2411e1ee4dfb9f22a861492d20c4765150c0c794abd70f8147c')
(16, 'ab5ab0fedc83e5a1a1871c427eccbcd3cf0fc1bb74a82a552adfd9b4e57f391b')
(32, '0aad7da77d2ed59c396c99a74e49f3a4524dcdbcb5163251b1433d640247aeb4')
(48, '2ac9a6746aca543af8dff39894cfe8173afba21eb01c6fae33d52947222855ef')
(64, '2ec847d8a31a988b3117a5095dae74f490448223f035ec7eddef6768b91a9028')
(80, '204164d223b35aabb54ea32b1d14d8bb5a8df56f7c81f3304987fa4193426729')
(96, 'f1b901847390b0ed7e374e7c1e464ec17b46a427c487a5ad6cbd2906405083d5')
(112, 'c4289629b08bc4d61411aaa6d6d4a0c3c5f8c1e848e282976e29b6bed5aeedc7')
(128, '8ae40a3583aef6697d2c2eff57eb915ed0bda54aaa92812ad97982743ac06f37')
(143, 'b9e8d0a22760b87553c0b9c55ae93058bf8d4389c87765488cea1637e94bd9b6')
(159, '91737e71235959a56c524997e18d6d14d6ddd714ed2a450a24f765255a2733ee')
(175, '700af1feb55ab0613bdbc466815643743156af4e869120244eb05ca72c45002c')
(191, 'ac205167ca956b408a925c3854fdd82ffa43672263ae7dba5a68b29d9a81fa56')
(207, '7b108f7c5c6f1507c4ffe2275dd9b8e25a71d175a5a9d3e19aeec3f27d82caf1')
(223, 'a30cb1d8569c5c141b2ade1caf57038b2be46c9bc4939c8f702a0ff4fcecfd77')
(239, '5ae0d5195906bfc4f70167cf171ae4d08e7376aa246977acf172187d5d384f10')
(255, '5ae7e6a42304dc6e4176210b83c43024f99a0bce9a870c3b6d2c95fc8ebfb74c')
#+END_EXAMPLE

Now we got all the pixels corresponding to the hashes in the encrypted text

Now Decryption , But they have not given the image resolution , there are ~7371~ pixels now the image resolution will be factor of this number

We try them all !!

#+BEGIN_SRC python :results output org drawer

key_dic = {'709e80c88487a2411e1ee4dfb9f22a861492d20c4765150c0c794abd70f8147c': 0,
           'ab5ab0fedc83e5a1a1871c427eccbcd3cf0fc1bb74a82a552adfd9b4e57f391b': 1,
           '0aad7da77d2ed59c396c99a74e49f3a4524dcdbcb5163251b1433d640247aeb4': 3,
           '2ac9a6746aca543af8dff39894cfe8173afba21eb01c6fae33d52947222855ef': 4,
           '2ec847d8a31a988b3117a5095dae74f490448223f035ec7eddef6768b91a9028': 6,
           '204164d223b35aabb54ea32b1d14d8bb5a8df56f7c81f3304987fa4193426729': 8,
           'f1b901847390b0ed7e374e7c1e464ec17b46a427c487a5ad6cbd2906405083d5': 9,
           'c4289629b08bc4d61411aaa6d6d4a0c3c5f8c1e848e282976e29b6bed5aeedc7': 11,
           '8ae40a3583aef6697d2c2eff57eb915ed0bda54aaa92812ad97982743ac06f37': 12,
           'b9e8d0a22760b87553c0b9c55ae93058bf8d4389c87765488cea1637e94bd9b6': 14,
           '91737e71235959a56c524997e18d6d14d6ddd714ed2a450a24f765255a2733ee': 15,
           '700af1feb55ab0613bdbc466815643743156af4e869120244eb05ca72c45002c': 17,
           'ac205167ca956b408a925c3854fdd82ffa43672263ae7dba5a68b29d9a81fa56': 19,
           '7b108f7c5c6f1507c4ffe2275dd9b8e25a71d175a5a9d3e19aeec3f27d82caf1': 20,
           'a30cb1d8569c5c141b2ade1caf57038b2be46c9bc4939c8f702a0ff4fcecfd77': 22,
           '5ae0d5195906bfc4f70167cf171ae4d08e7376aa246977acf172187d5d384f10': 23,
           '5ae7e6a42304dc6e4176210b83c43024f99a0bce9a870c3b6d2c95fc8ebfb74c': 25,
           }

pixels = []
num = 0
print(len(enc))
for i in enc:
    num = key_dic[i]
    pixels.append((num, num, num))


pos_cor = [(1, 7371),
           (3, 2457),
           (7, 1053),
           (9, 819),
           (13, 567),
           (21, 351),
           (27, 273),
           (39, 189),
           (63, 117),
           (81, 91)]

for j in pos_cor:
    img = Image.new('RGB', j, 'white')
    pix = img.load()
    cordinates = []

    for i in range(0, len(pixels), j[1]):
        cordinates.append(pixels[i:i + j[1]])

    for i in range(0, j[0]):
        for k, n in zip(cordinates[i], range(0, j[1])):
            pix[i, n] = k[0:3]
    img.save("image" + str(j[0]) + ".png")


#+END_SRC

~image21.png~ contains the flag
*** 2017-09-26 Tuesday
**** Report : 
***** Solving Bomb Labs
Phase 1 : 
The input is checked with the string ~Public speaking is very easy.~
Phase 2 : 
It reads 6 numbers and checks
#+BEGIN_SRC python :results output org drawer
input = [ 6 numbers ]
i = 1
for j in range(1,5):
    if i * j[i-1] == j[i]:
        continue
    else:
        return 1
return 0
    
#+END_SRC
Phase 3:
The input are one digit one charecter and one digit , also the first input sould be less than 7 , according to the offset of the fist input
the program is jumped to <input_1>*4+<addr_offset> at that location the char and the second number is checked accordingly , so as the first
number changes the constrains of other also changes .
Phase 4 : 
Inputs One digit and func4 and the return is checked with 0x37
#+BEGIN_SRC python :results output org drawer
def func4(a):
    if a <=1 :
            return 1
    else:
            return func4(a-1) + func4(a-2)
#+END_SRC

is the input is 9 
Phase 5:
it Reads 6 char strings and calculate logical and operaton on the letters and the result is used as an index for the substitution array  'isrveawhobpnutfg'
so the input is 'OPEKMA'

