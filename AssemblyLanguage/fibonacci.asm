	;; Reads a argument and print the nth value in fibanacci seriese 

	extern printf
	extern  atoi

	SECTION .data
msg1:	db " Fibanacci[%d] =  %d ",10,0
msg2:	db "%d",10,0

	SECTION .text
	global main

main:
	push ebp
	mov ebp,esp
	sub esp,0x10
	mov DWORD[ebp-0x8],-1
	mov DWORD[ebp-0xc],1
	mov DWORD [ebp-0x10],0


	mov eax , DWORD [ebp+0xc]
	add eax ,0x4
	mov eax ,DWORD [eax]
	push eax
	call atoi
	mov DWORD [ebp-0x4] , eax
	add esp , 0x4

fibanacci:
	mov edx ,DWORD [ebp-0xc]
	mov eax , DWORD[ebp-0x8]
	add eax , edx
	mov DWORD [ebp-0xc] , eax
	mov DWORD [ebp-0x8] , edx
	add DWORD [ebp-0x10], 0x1

	push DWORD [ebp-0xc]
	push msg2
	call printf
	add esp ,0x4
	
	mov eax , DWORD[ebp-0x10]
	cmp eax, DWORD [ebp-0x4]
	jl fibanacci
	
	push DWORD [ebp-0xc]
	push DWORD [ebp-0x4]
	push msg1
	call printf

	leave
	ret

	
	
