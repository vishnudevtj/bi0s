; program that prints a multiplication table for numbers up to 12.
	extern printf
	extern scanf

	SECTION .data
into:	db "Multiplication  table of %d",10,0
fmt:	db "%d * %d = %d ",10,0

	SECTION .text
	global main
	global mul
main:
	push ebp
	mov ebp,esp
	sub esp,0x4
	mov DWORD [ebp-0x4],0x1
loop1:	
	push DWORD [ebp-0x4]
	call mul
	add esp,0x4
	inc DWORD [ebp-0x4]

	jmp DWORD [ebp-0x4],0xc
	jle loop1
	
	leave
	ret
mul:
	push ebp
	mov ebp,esp
	sub esp,0x8

	push DWORD [ebp+0x8]
	push into
	call printf
	add esp,0x4

	mov DWORD [ebp-0x8],0x1
loop:	mov eax,[ebp+0x8]
	imul eax,DWORD [ebp-0x8]
	
	push eax
	push DWORD [ebp-0x8]
	push DWORD [ebp+0x8]
	push fmt
	call printf
	add esp,0x10

	inc DWORD [ebp-0x8]
	cmp DWORD [ebp-0x8],0xa
	jle loop
	
	mov eax,0x1
	leave
	ret
	
	

	
