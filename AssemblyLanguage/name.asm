	extern printf
	extern scanf
	SECTION .data
msg1:	db "Enter Your Name : ", 0
hello:	db "Hello  , %s",10, 0
fmt1:	db "%s" ,0
name:	db 0

SECTION .text
	global main

main:
	push ebp
	mov ebp,esp

	push msg1
	call printf
	add esp,0x4

	push name
	push fmt1
	call scanf
	add esp,0x8

	push name
	push hello
	call printf
	add esp,0x4
	
	
	leave
	ret
	
