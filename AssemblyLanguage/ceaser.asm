	;; Ceaser Cipher Reads the string and key from argument <string> <key>

	extern printf
	extern atoi

	SECTION .data

fmt:	db "%c",0

	SECTION .text
	global main		
main:
	push ebp
	mov ebp,esp
	sub esp,0x20
	mov DWORD [ebp-0x8],0
	mov eax , DWORD [ebp+0xc]
	add eax , 0x4
	mov eax ,DWORD [eax]
	mov DWORD [ebp-0xc], eax

	mov eax , DWORD [ebp+0xc]
	add eax , 0x8
	mov eax ,DWORD [eax]
	push eax
	call atoi
	mov DWORD [ebp-0x4] ,eax
	


loop:	mov edx , DWORD [ebp-0xc]
	sub edx , DWORD [ebp-0x8]
	movsx  eax , byte [edx]

	cmp eax,0x41
	jge con1
con2	cmp eax,0x61
	jge con3
	push eax
	jmp exp
	


con1:	cmp eax,0x5a
	jle ass1
	cmp eax,0x61
	jge con3

	push eax
	jmp exp

con3:	cmp eax ,0x7a
	jle ass2

ass1:	mov ecx,0x41
	jmp con
ass2:	mov ecx,0x61
	jmp con
con:
	sub eax ,ecx
	add  eax , [ebp-0x4]
	cdq
	mov ebx , 26
	idiv ebx
	add edx,ecx
	push edx
exp:	push fmt
	call printf
	sub DWORD [ebp-0x8] ,0x1
	mov edx , DWORD [ebp-0xc]
	sub edx , DWORD [ebp-0x8]
	movsx  eax , byte [edx] 
	cmp eax,0x0
	jne loop


	leave
	ret
