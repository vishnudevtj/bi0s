* Assembly Language
- System Organisation
** Memory

- Every Process is laid out in the same virtual memory space - regardless of the actual
of the physical memory location
- Every process feels that it is in the system alone and enjoys all the memory
and that no other process exists
- OS and the CPU together helps in maintaining this abstraction
- This is how memory Looks
#+BEGIN_EXAMPLE

 ,* Higher Memory Adress (0xBFFFFFFF)

    +-------------------+      Stacks Grows to Lower memory
    |      Stack        |               |
    |  	            |               |
    +-------------------+               +
    +-------------------+
    |  Unused           |
    |      Memory       |
    +-------------------+
    +-------------------+
    |       Heap        |     --> Dinamicaly allocated Memory
    |                   |
    +-------------------+
    +-------------------+
    |      .bss         |     -->  Uninitialized Data
    |                   |
    +-------------------+
    +-------------------+
    |     .data	    |     --> Initialized data
    |                   |
    +-------------------+
    +-------------------+
    |     .text	    |     --> Program Code
    |                   |
    +-------------------+

 ,* Lower Memory Address (0x8048000)

#+END_EXAMPLE

*** Stack 
- LIFO Memory { Last In First Out }
- Two Primary operation POP and PUSH 
  - POP Removes element from stack
  - Push Inserts element into the stack
- ESP Stack Pointer --> Points to the base of the stack 
  When a pop operation is performed the SP is Incremented
  When a Push operation is performed the SP is Decremented
  Since the stack grows from higher to lower memory

#+BEGIN_SRC C
void func(int a,int b,inc )
{
....
}

void main()
{
...
func()
...
}
#+END_SRC

Assembly Code 
#+BEGIN_SRC asm
	push c
	push b
	push a
	call func1

	push ebp
	mov ebp,esp
	sub esp,0xC

	....

	mov esp,ebp
	pop ebp

	ret
#+END_SRC
Stack Frame Of a Funtion
#+BEGIN_EXAMPLE
--->  Stack Frame Created on funtion call

+----------+
|   c	 |
+----------+
|   b	 |
+----------+
|   a	 |
+----------+
|saved_eip |
+----------+
|saved_ebp | 
+----------+
|local_var |
+----------+

#+END_EXAMPLE

* GDB 
GNU Debugger 
*** CheetSheet

[[file:./screen-shot-2012-05-01-at-ec98a4eca084-9-15-46.png]]
[[file:./screen-shot-2012-05-01-at-ec98a4eca084-9-15-59.png]]

** Writing First Assembly Program 

#+BEGIN_SRC asm :tangle ./hello.asm

section .text:
	global main

main:
	mov eax, 0x4
	mov ebx, 0x3
	sub eax,ebx

#+END_SRC

