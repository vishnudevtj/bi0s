/* A C program that takes a string as a command line argument and a number as a environment variable. Perform caesar cipher on the string with the environment variable acting as the rot. Keep a defaul 
   rot value of 13 */


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  int rot=13;
  if(argc<2)
    {
      printf("Usage %s <string> \n",argv[0]);
      return 0;
    }

  if(getenv("rot")!= '\0')
    {
      rot = atoi(getenv("rot"));
    }


  for(int i =0;argv[1][i]!='\0';i++)
    {
      if((int)argv[1][i]>=65 && (int)argv[1][i]<=90)
	{
	  printf("%c",((int)argv[1][i]-(int)'A'+rot)%26+(int)'A');
	}
      else if((int)argv[1][i]>=97 && (int)argv[1][i]<=122)
	{
	  printf("%c",((int)argv[1][i]-(int)'a'+rot)%26+(int)'a');
	}
      else
	{
	  printf("%c",argv[1][i]);
	}
    }
}
