	extern scanf
	extern printf

	SECTION .data

msg:	db "Enter the number : " ,0
fmt:	db "%d",0
fmt1:	db "Sum of numbers from 1 to %d : %d ",10 ,0 

	SECTION .text
	global main
main:

	push ebp
	mov ebp,esp
	sub esp,0x8

	push msg
	call printf
	add esp,0x4

	lea eax,[ebp-0x4]
	push eax
	push fmt
	call scanf
	add esp,0x8

	xor ecx,ecx
	mov DWORD[ebp-0x8],0x0
	
loop:	inc ecx
	add DWORD [ebp-0x8],ecx
	cmp ecx, DWORD [ebp-0x4]
	jne loop

	push DWORD [ebp-0x8]
	push DWORD [ebp-0x4]
	push fmt1
	call printf
	add esp,0xc



	leave
	ret
	
