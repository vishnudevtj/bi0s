	extern printf
	extern getenv
	extern environ
	SECTION .data
fmt1:	db "%s"	,10,0

	SECTION .text

	global main
main:
	push ebp
	mov ebp,esp
	sub esp,0x4
	mov DWORD [ebp-0x4],0x0

	cmp DWORD [ebp+0x8],0x2
	jge env

loop:	mov eax, DWORD [environ]
	add eax,DWORD [ebp-0x4]
	push DWORD [eax]
	push fmt1
	call printf
	add DWORD [ebp-0x4] ,0x4
	mov eax , DWORD [ebp-0x8]
	add esp,0x8
	cmp eax,0
	jne loop
	leave
	ret

env:	mov eax, DWORD [ebp+0xc]
	push DWORD [eax+0x4]
	call getenv
	add esp,0x4
	push eax
	push fmt1
	call printf
	add esp,0x8


	leave
	ret
	
